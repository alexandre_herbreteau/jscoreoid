package test;

import java.util.List;

import com.jscoreoid.api.game.GameFacade;
import com.jscoreoid.api.player.PlayerFacade;
import com.jscoreoid.api.score.ScoreFacade;
import com.jscoreoid.exceptions.RequestException;
import com.jscoreoid.model.Game;
import com.jscoreoid.model.GameFields;
import com.jscoreoid.model.GameStatFields;
import com.jscoreoid.model.Notification;
import com.jscoreoid.model.OrderBy;
import com.jscoreoid.model.Player;
import com.jscoreoid.model.PlayerFields;
import com.jscoreoid.model.Score;
import com.jscoreoid.model.ScoreOrderBy;

public class Main {
	
	public static void main(String[] args) {
		
		/* Create a ScoreoidConfig interface with this fields: 
		 * 	static final String apiURL = "https://www.scoreoid.com/api/" ;
			static final String scoreoidID = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" ;
			static final String gameID = "XXXXXXXXXX" ;
		 */
		
		/* GAME */
		GameFacade gameF = new GameFacade(ScoreoidConfig.apiURL, ScoreoidConfig.scoreoidID);
		
		/* GetGame */
		Game aGame = gameF.getGame(ScoreoidConfig.gameID);
		System.out.println("GAME - getGame - name : " + aGame.getName());
		System.out.println("GAME - getGame - user id : " + aGame.getUserId());
		System.out.println("GAME - getGame - short description : " + aGame.getShortDescription());
		System.out.println("GAME - getGame - description : " + aGame.getDescription());
		System.out.println("GAME - getGame - game type : " + aGame.getGameType());
		System.out.println("GAME - getGame - version : " + aGame.getVersion());
		System.out.println("GAME - getGame - levels : " + aGame.getLevels());
		System.out.println("GAME - getGame - platform : " + aGame.getPlatform());
		System.out.println("GAME - getGame - play URL : " + aGame.getPlayUrl());
		System.out.println("GAME - getGame - website URL : " + aGame.getWebsiteUrl());
		System.out.println("GAME - getGame - created : " + aGame.getCreated());
		System.out.println("GAME - getGame - updated : " + aGame.getUpdated());
		System.out.println("GAME - getGame - players count : " + aGame.getPlayersCount());
		System.out.println("GAME - getGame - scores count : " + aGame.getScoresCount());
		System.out.println("GAME - getGame - locked : " + aGame.getLocked());
		System.out.println("GAME - getGame - status : " + aGame.getStatus());	
		
		/* GetGameField */
		System.out.println("GAME - getGameField - name : " + gameF.getGameField(ScoreoidConfig.gameID, GameFields.Name));
		System.out.println("GAME - getGameField - user id : " + gameF.getGameField(ScoreoidConfig.gameID, GameFields.UserId));
		System.out.println("GAME - getGameField - short description : " + gameF.getGameField(ScoreoidConfig.gameID, GameFields.ShortDescription));
		System.out.println("GAME - getGameField - description : " + gameF.getGameField(ScoreoidConfig.gameID, GameFields.Description));	
		System.out.println("GAME - getGameField - version : " + gameF.getGameField(ScoreoidConfig.gameID, GameFields.Version));
		System.out.println("GAME - getGameField - levels : " + gameF.getGameField(ScoreoidConfig.gameID, GameFields.Levels));
		System.out.println("GAME - getGameField - platform : " + gameF.getGameField(ScoreoidConfig.gameID, GameFields.Platform));
		System.out.println("GAME - getGameField - play URL : " + gameF.getGameField(ScoreoidConfig.gameID, GameFields.PlayURL));
		System.out.println("GAME - getGameField - website URL : " + gameF.getGameField(ScoreoidConfig.gameID, GameFields.WebsiteURL));
		System.out.println("GAME - getGameField - created : " + gameF.getGameField(ScoreoidConfig.gameID, GameFields.Created));
		System.out.println("GAME - getGameField - updated : " + gameF.getGameField(ScoreoidConfig.gameID, GameFields.Updated));
		System.out.println("GAME - getGameField - players count : " + gameF.getGameField(ScoreoidConfig.gameID, GameFields.PlayersCount));
		System.out.println("GAME - getGameField - scores count : " + gameF.getGameField(ScoreoidConfig.gameID, GameFields.ScoresCount));
		System.out.println("GAME - getGameField - locked : " + gameF.getGameField(ScoreoidConfig.gameID, GameFields.Locked));
		System.out.println("GAME - getGameField - status : " + gameF.getGameField(ScoreoidConfig.gameID, GameFields.Status));
				
		/* GetPlayers */		
		List<Player> jplayers = gameF.getPlayers(ScoreoidConfig.gameID);
		System.out.println("GAME - getPlayers - bestScore : " + jplayers.get(0).getBestScore());
		
		/* GetGameAverage */
		System.out.println("GAME - getGameAverage - bonus : " + gameF.getGameAverage(ScoreoidConfig.gameID, GameStatFields.Bonus));
		System.out.println("GAME - getGameAverage - gold : " + gameF.getGameAverage(ScoreoidConfig.gameID, GameStatFields.Gold));
		System.out.println("GAME - getGameAverage - kills : " + gameF.getGameAverage(ScoreoidConfig.gameID, GameStatFields.Kills));
		System.out.println("GAME - getGameAverage - lives : " + gameF.getGameAverage(ScoreoidConfig.gameID, GameStatFields.Lives));
		System.out.println("GAME - getGameAverage - money : " + gameF.getGameAverage(ScoreoidConfig.gameID, GameStatFields.Money));
		System.out.println("GAME - getGameAverage - time played : " + gameF.getGameAverage(ScoreoidConfig.gameID, GameStatFields.TimePlayed));
		System.out.println("GAME - getGameAverage - unlocked levels : " + gameF.getGameAverage(ScoreoidConfig.gameID, GameStatFields.UnlockedLevels));
		
		/* getGameTop */
		System.out.println("GAME - getGameTop - bonus : " + gameF.getGameTop(ScoreoidConfig.gameID, GameStatFields.Bonus));
		System.out.println("GAME - getGameTop - gold : " + gameF.getGameTop(ScoreoidConfig.gameID, GameStatFields.Gold));
		System.out.println("GAME - getGameTop - kills : " + gameF.getGameTop(ScoreoidConfig.gameID, GameStatFields.Kills));
		System.out.println("GAME - getGameTop - lives : " + gameF.getGameTop(ScoreoidConfig.gameID, GameStatFields.Lives));
		System.out.println("GAME - getGameTop - money : " + gameF.getGameTop(ScoreoidConfig.gameID, GameStatFields.Money));
		System.out.println("GAME - getGameTop - time played : " + gameF.getGameTop(ScoreoidConfig.gameID, GameStatFields.TimePlayed));
		System.out.println("GAME - getGameTop - unlocked levels : " + gameF.getGameTop(ScoreoidConfig.gameID, GameStatFields.UnlockedLevels));
		
		/* getGameLowest */
		System.out.println("GAME - getGameLowest - bonus : " + gameF.getGameLowest(ScoreoidConfig.gameID, GameStatFields.Bonus));
		System.out.println("GAME - getGameLowest - gold : " + gameF.getGameLowest(ScoreoidConfig.gameID, GameStatFields.Gold));
		System.out.println("GAME - getGameLowest - kills : " + gameF.getGameLowest(ScoreoidConfig.gameID, GameStatFields.Kills));
		System.out.println("GAME - getGameLowest - lives : " + gameF.getGameLowest(ScoreoidConfig.gameID, GameStatFields.Lives));
		System.out.println("GAME - getGameLowest - money : " + gameF.getGameLowest(ScoreoidConfig.gameID, GameStatFields.Money));
		System.out.println("GAME - getGameLowest - time played : " + gameF.getGameLowest(ScoreoidConfig.gameID, GameStatFields.TimePlayed));
		System.out.println("GAME - getGameLowest - unlocked levels : " + gameF.getGameLowest(ScoreoidConfig.gameID, GameStatFields.UnlockedLevels));
		
		/* getGameTotal */
		System.out.println("GAME - getGameTotal - bonus : " + gameF.getGameTotal(ScoreoidConfig.gameID, GameStatFields.Bonus));
		System.out.println("GAME - getGameTotal - gold : " + gameF.getGameTotal(ScoreoidConfig.gameID, GameStatFields.Gold));
		System.out.println("GAME - getGameTotal - kills : " + gameF.getGameTotal(ScoreoidConfig.gameID, GameStatFields.Kills));
		System.out.println("GAME - getGameTotal - lives : " + gameF.getGameTotal(ScoreoidConfig.gameID, GameStatFields.Lives));
		System.out.println("GAME - getGameTotal - money : " + gameF.getGameTotal(ScoreoidConfig.gameID, GameStatFields.Money));
		System.out.println("GAME - getGameTotal - time played : " + gameF.getGameTotal(ScoreoidConfig.gameID, GameStatFields.TimePlayed));
		System.out.println("GAME - getGameTotal - unlocked levels : " + gameF.getGameTotal(ScoreoidConfig.gameID, GameStatFields.UnlockedLevels));
		
		/* setGameData */
		System.out.println("GAME - setGameData success : " + gameF.setGameData(ScoreoidConfig.gameID, "foo", "value")) ;
		
		/* getGameData */
		System.out.println("GAME - getGameData : " + gameF.getGameData(ScoreoidConfig.gameID, "foo")) ;
		System.out.println("GAME - getGameData with default value: " + gameF.getGameData(ScoreoidConfig.gameID, "foo2", "zee")) ;
		
		/* unsetGameData */
		System.out.println("GAME - unsetGameData success : " + gameF.unsetGameData(ScoreoidConfig.gameID, "foo")) ;
		
		/* getNotification */
		List<Notification> notifications = gameF.getNotification(ScoreoidConfig.gameID);
		System.out.println("GAME - getNotification title : " + notifications.get(0).getTitle()) ;
		System.out.println("GAME - getNotification content : " + notifications.get(0).getContent()) ;
		System.out.println("GAME - getNotification start date : " + notifications.get(0).getStartDate()) ;
		System.out.println("GAME - getNotification end date : " + notifications.get(0).getEndDate()) ;
		System.out.println("GAME - getNotification debug : " + notifications.get(0).isDebug()) ;
		System.out.println("GAME - getNotification status : " + notifications.get(0).isStatus()) ;		
		
		/* SCORES */
		ScoreFacade scoreF = new ScoreFacade(ScoreoidConfig.apiURL, ScoreoidConfig.scoreoidID);
		
		/* createScore */
		//System.out.println("SCORE - createScore 1 result: " + score.createScore(ScoreoidConfig.gameID, "totoTest", 100));
		//System.out.println("SCORE - createScore 2 result: " + score.createScore(ScoreoidConfig.gameID, "totoTest", 99, "Android", "", 3, "aaabbbb"));
		
		/* CountScores */
		System.out.println("SCORE - countScores 1 :" + scoreF.countScores(ScoreoidConfig.gameID));
		System.out.println("SCORE - countScores 2 :" + scoreF.countScores(ScoreoidConfig.gameID, "", "", "Android", 3));
		
		/* CountBestScores */
		System.out.println("SCORE - countBestScores 1 :" + scoreF.countBestScores(ScoreoidConfig.gameID));
		System.out.println("SCORE - countBestScores 2 :" + scoreF.countBestScores(ScoreoidConfig.gameID, "", "", "", 3));
		
		/* getScores */
		List<Score> jscores = scoreF.getScores(ScoreoidConfig.gameID);
		System.out.println("SCORE - getScores 1 - player username :" + jscores.get(0).getPlayer().getUsername());
		System.out.println("SCORE - getScores 1 - score  :" + jscores.get(0).getScore());
		System.out.println("SCORE - getScores 1 - difficulty  :" + jscores.get(0).getDifficulty());
		System.out.println("SCORE - getScores 1 - platform  :" + jscores.get(0).getPlatform());
		
		List<Score> jscores2 = scoreF.getScores(ScoreoidConfig.gameID, ScoreOrderBy.SCORE, OrderBy.DESC, 2, 1, "", "", "", 0, null);
		System.out.println("SCORE - getScores 2 - player username :" + jscores2.get(0).getPlayer().getUsername());
		System.out.println("SCORE - getScores 2 - score  :" + jscores2.get(0).getScore());
		System.out.println("SCORE - getScores 2 - difficulty  :" + jscores2.get(0).getDifficulty());
		System.out.println("SCORE - getScores 2 - platform  :" + jscores2.get(0).getPlatform());
		
		/* getBestScores */
		List<Score> jbestScores = scoreF.getBestScores(ScoreoidConfig.gameID);
		System.out.println("SCORE - getBestScores - player username :" + jbestScores.get(0).getPlayer().getUsername());
		System.out.println("SCORE - getBestScores - score  :" + jbestScores.get(0).getScore());
		System.out.println("SCORE - getBestScores - difficulty  :" + jbestScores.get(0).getDifficulty());
		System.out.println("SCORE - getBestScores - platform  :" + jbestScores.get(0).getPlatform());
		
		/* getAverageScore */
		System.out.println("SCORE - getAverageScore : " + scoreF.getAverageScore(ScoreoidConfig.gameID));
		
		/* PLAYER */
		PlayerFacade playerF = new PlayerFacade(ScoreoidConfig.apiURL, ScoreoidConfig.scoreoidID);
		
		/* createPlayer */
		try{
			System.out.println("PLAYER - createPlayer: " + playerF.createPlayer(ScoreoidConfig.gameID, "totoTest3", "essai"));
		}catch(RequestException re){
			re.printStackTrace();
		}
		
		/* editPlayer */  
		final String playerName = "totoTest2" ;
		Player aPlayer = new Player() ;
		aPlayer.setUsername(playerName);
		aPlayer.setScore(30);
		aPlayer.setDifficulty(3);
		aPlayer.setFirstName("Jambon");
		aPlayer.setLastName("Aost");
		aPlayer.setEmail("jbaost@grec.com");
		aPlayer.setBonus(1);
		aPlayer.setBestScore(80);
		aPlayer.setTimePlayed(330);
		aPlayer.setLastLevel(13);
		aPlayer.setCurrentLevel(5);
		aPlayer.addCurrentAchievements("Achvmnt1");
		aPlayer.addCurrentAchievements("Achvmnt2");
		aPlayer.addCurrentAchievements("Achvmnt3");
		
		System.out.println("PLAYER - editPlayer success: " + playerF.editPlayer(	ScoreoidConfig.gameID, aPlayer ));
		
		/* deletePlayer */
		System.out.println("PLAYER - deletePlayer success: " + playerF.deletePlayer(	ScoreoidConfig.gameID, "totoTest3" ));
		
		/* getPlayer */
		Player aPlayer2 = playerF.getPlayer(ScoreoidConfig.gameID, playerName ) ;
		System.out.println("PLAYER - getPlayer username: " + aPlayer2.getUsername());
		System.out.println("PLAYER - getPlayer score: " + aPlayer2.getScore());
		System.out.println("PLAYER - getPlayer difficulty: " + aPlayer2.getDifficulty());
		System.out.println("PLAYER - getPlayer first name: " + aPlayer2.getFirstName());
		System.out.println("PLAYER - getPlayer last name: " + aPlayer2.getLastName());
		System.out.println("PLAYER - getPlayer email: " + aPlayer2.getEmail());
		System.out.println("PLAYER - getPlayer bonus: " + aPlayer2.getBonus());
		System.out.println("PLAYER - getPlayer best score: " + aPlayer2.getBestScore());
		System.out.println("PLAYER - getPlayer time played: " + aPlayer2.getTimePlayed());
		System.out.println("PLAYER - getPlayer last level: " + aPlayer2.getLastLevel());
		System.out.println("PLAYER - getPlayer current level: " + aPlayer2.getCurrentLevel());
		List<String> currentAchievements = aPlayer2.getCurrentAchievements() ;
		System.out.println("PLAYER - getPlayer current achievement 1: " + currentAchievements.get(0));
		System.out.println("PLAYER - getPlayer current achievement 2: " + currentAchievements.get(1));
		System.out.println("PLAYER - getPlayer current achievement 3: " + currentAchievements.get(2));
		
		/* getPlayerField */		
		System.out.println("PLAYER - getPlayerField username: " + playerF.getPlayerField(ScoreoidConfig.gameID, playerName, PlayerFields.Username));		
		System.out.println("PLAYER - getPlayerField first name: " + playerF.getPlayerField(ScoreoidConfig.gameID, playerName, PlayerFields.FirstName));
		System.out.println("PLAYER - getPlayerField last name: " + playerF.getPlayerField(ScoreoidConfig.gameID, playerName, PlayerFields.LastName));
		System.out.println("PLAYER - getPlayerField email: " + playerF.getPlayerField(ScoreoidConfig.gameID, playerName, PlayerFields.Email));
		System.out.println("PLAYER - getPlayerField best score: " + playerF.getPlayerField(ScoreoidConfig.gameID, playerName, PlayerFields.BestScore));
		System.out.println("PLAYER - getPlayerField time played: " + playerF.getPlayerField(ScoreoidConfig.gameID, playerName, PlayerFields.TimePlayed));
		System.out.println("PLAYER - getPlayerField current achivements: " + playerF.getPlayerField(ScoreoidConfig.gameID, playerName, PlayerFields.CurrentAchievements));
		
		/* getPlayerRank */
		System.out.println("PLAYER - getPlayerRank : " + playerF.getPlayerRank(ScoreoidConfig.gameID, playerName));
		
		/* updatePlayerField */
		System.out.println("PLAYER - updatePlayerField success: " + playerF.updatePlayerField(ScoreoidConfig.gameID, playerName, PlayerFields.Bonus, "44"));
		System.out.println("PLAYER - updatePlayerField success: " + playerF.updatePlayerField(ScoreoidConfig.gameID, playerName, PlayerFields.TimePlayed, "350"));
		// -----
		System.out.println("PLAYER - getPlayerField bonus: " + playerF.getPlayerField(ScoreoidConfig.gameID, playerName, PlayerFields.Bonus));
		System.out.println("PLAYER - getPlayerField time played: " + playerF.getPlayerField(ScoreoidConfig.gameID, playerName, PlayerFields.TimePlayed));
		
		/* getScores */
		List<Score> scores = playerF.getPlayerScores(ScoreoidConfig.gameID, "totoTest");
		System.out.println("PLAYER - getPlayerScores id: " + scores.get(0).getID()) ;
		System.out.println("PLAYER - getPlayerScores game id: " + scores.get(0).getGameID()) ;
		System.out.println("PLAYER - getPlayerScores player id: " + scores.get(0).getPlayerID()) ;
		System.out.println("PLAYER - getPlayerScores score: " + scores.get(0).getScore()) ;
		System.out.println("PLAYER - getPlayerScores difficulty: " + scores.get(0).getDifficulty()) ;
		System.out.println("PLAYER - getPlayerScores platform: " + scores.get(0).getPlatform()) ;
		System.out.println("PLAYER - getPlayerScores created: " + scores.get(0).getCreated()) ;
		System.out.println("PLAYER - getPlayerScores data: " + scores.get(0).getData()) ;
		
		/* countPlayers */
		System.out.println("PLAYER - countPlayers : " + playerF.countPlayers(ScoreoidConfig.gameID)) ;
		
		/* setPlayerData */
		System.out.println("PLAYER - setPlayerData success : " + playerF.setPlayerData(ScoreoidConfig.gameID, playerName, "foo", "value")) ;
		
		/* getPlayerData */
		System.out.println("PLAYER - getPlayerData : " + playerF.getPlayerData(ScoreoidConfig.gameID, playerName, "foo")) ;
		
		/* unsetPlayerData */
		System.out.println("PLAYER - unsetPlayerData success : " + playerF.unsetPlayerData(ScoreoidConfig.gameID, playerName, "foo")) ;
	}
}
