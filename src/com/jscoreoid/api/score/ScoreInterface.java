package com.jscoreoid.api.score;

import java.util.List;

import com.jscoreoid.exceptions.RequestException;
import com.jscoreoid.model.OrderBy;
import com.jscoreoid.model.Player;
import com.jscoreoid.model.Score;
import com.jscoreoid.model.ScoreOrderBy;


/**
 * Interface for the Score API operations.
 * 
 * @author Alejandro Martínez Vieites
 */
public interface ScoreInterface {

    /**
     * Creates a new score for the player.
     * 
     * @param gameId the game identifier
     * @param score actual score
     * @param username username of the player who did the score
     * @param platform (optional)
     * @param uniqueId (optional)
     * @param difficulty (optional)
     * @param data (optional)
     * @return true, if the score has been created
     */
    boolean createScore(String gameId, String username, long score, String platform, String uniqueId, int difficulty, String data) throws RequestException;
    boolean createScore(String gameId, String username, long score) throws RequestException ;
    
    /**
     * Returns total number of scores for the game.
     * 
     * @param gameId the game identifier
     * @param startDate (optional)
     * @param endDate (optional)
     * @param platform (optional)
     * @param difficulty (optional)
     * @return the number of scores
     * @throws RequestException if something goes wrong
     */
    long countScores(String gameId, String startDate, String endDate, String platform, int difficulty) throws RequestException;
    long countScores(String gameId) throws RequestException;
    
    /**
     * Returns total number of best scores for the game.
     * 
     * @param gameId the game identifier
     * @param startDate (optional)
     * @param endDate (optional)
     * @param platform (optional)
     * @param difficulty (optional)
     * @return the number of scores
     * @throws RequestException if something goes wrong
     */
    long countBestScores(String gameId, String startDate, String endDate, String platform, int difficulty) throws RequestException;
    long countBestScores(String gameId) throws RequestException;

    /**
     * This method is pulling all the scores for a game.
     * <p>
     * The {@link Player} instance does not contain all the information of the player but only the
     * following data:
     * <ul>
     * <li>username</li>
     * <li>email</li>
     * <li>first name</li>
     * <li>last name</li>
     * <li>platform</li>
     * </ul>
     * The rest of the {@link Player}s data will not be filled.
     * </p>
     * 
     * @param gameId the game identifier
     * @param orderBy field to order by (optional)
     * @param order asc, desc, asc-asc, asc-desc, desc-desc, desc-asc (optional)
     * @param lowLimit 10 from "10,20" retrieves 20 scores starting from the 10th (optional)
     * @param numberofScore 20 retrieves rows 1 - 20 (optional)
     * @param startDate (optional)
     * @param endDate (optional)
     * @param platform (optional)
     * @param difficulty (optional)
     * @param usernames (optional)
     * @return the list of scores
     * @throws RequestException if something go wrong
     */
    List<Score> getScores(String gameId, ScoreOrderBy orderBy, OrderBy order, long lowLimit, int numberOfScore, String startDate, String endDate, String platform, int difficulty, List<String> usernames) throws RequestException;
    List<Score> getScores(String gameId) throws RequestException;
    
    /**
     * This method is pulling all the best scores for a game.
     * 
     * <p>
     * The {@link Player} instance does not contain all the information of the player but only the
     * following data:
     * <ul>
     * <li>username</li>
     * <li>email</li>
     * <li>first name</li>
     * <li>last name</li>
     * <li>platform</li>
     * </ul>
     * The rest of the {@link Player}s data will not be filled.
     * </p>
     * 
     * @param gameId the game identifier
     * @param orderBy field to order by (optional)
     * @param order asc, desc, asc-asc, asc-desc, desc-desc, desc-asc (optional)
     * @param lowLimit 10 from "10,20" retrieves 20 scores starting from the 10th (optional)
     * @param numberofScore 20 retrieves rows 1 - 20 (optional)
     * @param startDate (optional)
     * @param endDate (optional)
     * @param platform (optional)
     * @param difficulty (optional)
     * @param usernames (optional)
     * @return the list of scores
     * @throws RequestException if something go wrong
     */
    List<Score> getBestScores(String gameId, ScoreOrderBy orderBy, OrderBy order, long lowLimit, int numberOfScore, String startDate, String endDate, String platform, int difficulty, List<String> usernames) throws RequestException;
    List<Score> getBestScores(String gameId) throws RequestException;
    
    /**
     * Returns the average score for the game.
     * 
     * @param gameId the game identifier
     * @param startDate (optional)
     * @param endDate (optional)
     * @param platform (optional)
     * @param difficulty (optional)
     * @return the average score
     * @throws RequestException if something go wrong
     */
    double getAverageScore(String gameId, String startDate, String endDate, String platform, int difficulty) throws RequestException;
    double getAverageScore(String gameId) throws RequestException;
}
