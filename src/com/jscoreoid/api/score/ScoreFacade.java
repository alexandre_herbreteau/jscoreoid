package com.jscoreoid.api.score;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.jscoreoid.api.AbstractFacade;
import com.jscoreoid.exceptions.RequestException;
import com.jscoreoid.http.Petition;
import com.jscoreoid.model.EnumToField;
import com.jscoreoid.model.OrderBy;
import com.jscoreoid.model.Player;
import com.jscoreoid.model.Score;
import com.jscoreoid.model.ScoreOrderBy;
import com.jscoreoid.model.Tools;


/**
 * Implementation of the interface required to ask the scoreoid Score API.
 * 
 * @author Alejandro Martinez Vieites
 * @author Alexandre Herbreteau
 */
public class ScoreFacade extends AbstractFacade implements ScoreInterface {

    /**
     * Constructor for asking the Scores part of the API.
     * 
     * @param url the URL of the scoreoid API (https://www.scoreoid.com/api/)
     * @param apiKey the API key scoreoid gave to you
     */
    public ScoreFacade(String url, String apiKey) {
        super(url, apiKey);
    }

    @Override
    public boolean createScore(String gameId, String username, long score) throws RequestException {
    	return createScore(gameId, username, score, "", "", -1, "");
    }
    
    public boolean createScore(String gameId, String username, long score, String platform, String uniqueId, int difficulty, String data) throws RequestException{
        Petition request = null;
        String value = null;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "createScore", this.apiKey);
            value = gameId;            		request.addParameter("game_id", value);            
            value = "json";            		request.addParameter("response", value);
            value = String.valueOf(score);  request.addParameter("score", value);
            value = username;           	request.addParameter("username", value);
            value = uniqueId ;				request.addParameter("unique_id", value, true);
            value = platform ;	        	request.addParameter("platform", value, true);
            if ( difficulty != 0 )            	
            	value = Tools.NumericToString(difficulty);		request.addParameter("difficulty", value, true);            
            value = data ;		           request.addParameter("data", value, true);
            
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "createScore", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value: " + value, e);
        }

        // Send and get answer
        return Tools.getBooleanAnswer(request, client);
    }

    @Override
    public long countScores(String gameId) throws RequestException {    	
    	return countScores(gameId, "", "", "", -1);
    }
    
    public long countScores(String gameId, String startDate, String endDate, String platform, int difficulty) throws RequestException {
        Petition request = null;
        String rawResponse = null;
        String value = null ;
        long response = -1l;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "countScores", this.apiKey);
            value = gameId;					request.addParameter("game_id", value);
            value = "json" ; 				request.addParameter("response", value);
            value = startDate ;				request.addParameter("start_date", value, true);
            value = endDate ;				request.addParameter("end_date", value, true);
            value = platform ;				request.addParameter("platform", value, true);
            if ( difficulty != 0 )            	
            	value = Tools.NumericToString(difficulty);	request.addParameter("difficulty", value, true);                      
            
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "countScores", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value " + value, e);
        }

        // Send the request and get the JSON response.
        try {
            rawResponse = this.client.doPost(request);
        } catch (IOException e) {
            throw new RequestException("Something went wrong sending the request", e);
        }

        // Map the JSON response to the model object.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode;
        JsonNode childNode = null;

        try {
            rootNode = mapper.readTree(rawResponse);

            childNode = rootNode.findValue("scores");
            if (childNode != null) {
                response = childNode.asLong();
            } else {
                childNode = rootNode.findValue("error");
                if (childNode != null) {
                    throw new RequestException(rootNode.findValue("error").toString());
                }
            }
        } catch (JsonProcessingException e) {
            throw new RequestException("JSON error", e);
        } catch (IOException e) {
            throw new RequestException("IO error", e);
        }

        return response;
    }

    public long countBestScores(String gameId) throws RequestException {
    	return countBestScores(gameId, "", "", "", -1);
    }
    
    public long countBestScores(String gameId, String startDate, String endDate, String platform, int difficulty) throws RequestException {

        Petition request = null;
        String rawResponse = null;
        String value = null ;
        long response = -1l;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "countBestScores", this.apiKey);
            value = gameId ; 		request.addParameter("game_id", value);
            value = "json";			request.addParameter("response", value);
            value = startDate ; 	request.addParameter("start_date", value, true);
            value = endDate ; 		request.addParameter("end_date", value, true);
            value = platform ; 		request.addParameter("platform", value, true);
            if ( difficulty != 0 )            	
            	value = Tools.NumericToString(difficulty);	request.addParameter("difficulty", value, true);             
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "countBestScores", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value", e);
        }

        // Send the request and get the JSON response.
        try {
            rawResponse = this.client.doPost(request);
        } catch (IOException e) {
            throw new RequestException("Something went wrong sending the request", e);
        }

        // Map the JSON response to the model object.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode;
        JsonNode childNode = null;

        try {
            rootNode = mapper.readTree(rawResponse);

            childNode = rootNode.findValue("scores"); // FIXME best_scores when Scoreoid modification will be done
            if (childNode != null) {
                response = childNode.asLong();
            } else {
                childNode = rootNode.findValue("error");
                if (childNode != null) {
                    throw new RequestException(rootNode.findValue("error").toString());
                }
            }
        } catch (JsonProcessingException e) {
            throw new RequestException("JSON error", e);
        } catch (IOException e) {
            throw new RequestException("IO error", e);
        }

        return response;
    }

    @Override
    public List<Score> getScores(String gameId) throws RequestException {    	
    	return getScores(gameId, ScoreOrderBy.SCORE, OrderBy.DESC, -1, -1, "", "", "", -1, null);
    }
    
    public List<Score> getScores(	String gameId, ScoreOrderBy orderBy, OrderBy order, long lowLimit, int numberOfScore, 
    								String startDate, String endDate, String platform, int difficulty, List<String> usernames) throws RequestException {
        Petition request = null;
        String rawResponse = null;
        String value = null ;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "getScores", this.apiKey);
            value = gameId ; 						request.addParameter("game_id", value);
            value = "json";							request.addParameter("response", value);
            value = EnumToField.getValue(orderBy);	request.addParameter("order_by", value, true);
            value = EnumToField.getValue(order);	request.addParameter("order", value, true);
            if ( lowLimit == -1 )
            	value = Tools.NumericToString(numberOfScore);            	
            else
            	value = String.valueOf(lowLimit) + "," + String.valueOf(numberOfScore); 
            										request.addParameter("limit", value, true);
            value = startDate ; 					request.addParameter("start_date", value, true);
            value = endDate ; 						request.addParameter("end_date", value, true);
            value = platform ; 						request.addParameter("platform", value, true);
            if ( difficulty != 0 )            	
            	value = Tools.NumericToString(difficulty);	request.addParameter("difficulty", value, true);
            value =  Tools.ListToString(usernames);	request.addParameter("usernames", value, true);
            
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "getScores", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value", e);
        }

        // Send the request and get the JSON response.
        try {
            rawResponse = this.client.doPost(request);
        } catch (IOException e) {
            throw new RequestException("Something went wrong sending the request", e);
        }

        // Map the JSON response to the model object.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode;
        JsonNode childNode = null;
        List<Score> scores = new ArrayList<Score>();
        List<Player> players = new ArrayList<Player>();
        List<JsonNode> nodes = null;

        try {
            rootNode = mapper.readTree(rawResponse);

            childNode = rootNode.findValue("error");
            if (childNode != null) {
                if (childNode != null) {
                    throw new RequestException(rootNode.findValue("error").toString());
                }
            } else {
                nodes = rootNode.findValues("Player");
                for (JsonNode node : nodes) {
                    players.add(mapper.readValue(node, Player.class));
                }
                nodes = rootNode.findValues("Score");
                for (int i = 0; i < nodes.size(); i++) {
                    JsonNode node = nodes.get(i);
                    Score score = mapper.readValue(node, Score.class);
                    score.setPlayer(players.get(i));
                    scores.add(score);
                }
            }
        } catch (JsonProcessingException e) {
            throw new RequestException("JSON error", e);
        } catch (IOException e) {
            throw new RequestException("IO error", e);
        }
        return scores;
    }

    @Override
    public List<Score> getBestScores(String gameId) throws RequestException {
    	return getBestScores(gameId, ScoreOrderBy.SCORE, OrderBy.DESC, -1, -1, "", "", "", -1, null);
    }
    
    public List<Score> getBestScores(	String gameId, ScoreOrderBy orderBy, OrderBy order, long lowLimit, int numberOfScore, 
    									String startDate, String endDate, String platform, int difficulty, List<String> usernames) throws RequestException {
        Petition request = null;
        String rawResponse = null;
        String value = null ;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "getBestScores", this.apiKey);
            value = gameId ; 						request.addParameter("game_id", value);
            value = "json";							request.addParameter("response", value);
            value = EnumToField.getValue(orderBy);	request.addParameter("order_by", value, true);
            value = EnumToField.getValue(order);	request.addParameter("order", value, true);
            if ( lowLimit == -1 )
            	value = Tools.NumericToString(numberOfScore);            	
            else
            	value = String.valueOf(lowLimit) + "," + String.valueOf(numberOfScore); 
            										request.addParameter("limit", value, true);
            value = startDate ; 					request.addParameter("start_date", value, true);
            value = endDate ; 						request.addParameter("end_date", value, true);
            value = platform ; 						request.addParameter("platform", value, true);
            if ( difficulty != 0 )            	
            	value = Tools.NumericToString(difficulty);	request.addParameter("difficulty", value, true);
            value =  Tools.ListToString(usernames);	request.addParameter("usernames", value, true);
            
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "getBestScores", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value", e);
        }

        // Send the request and get the JSON response.
        try {
            rawResponse = this.client.doPost(request);
        } catch (IOException e) {
            throw new RequestException("Something went wrong sending the request", e);
        }

        // Map the JSON response to the model object.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode;
        JsonNode childNode = null;
        List<Score> scores = new ArrayList<Score>();
        List<Player> players = new ArrayList<Player>();
        List<JsonNode> nodes = null;

        try {
            rootNode = mapper.readTree(rawResponse);

            childNode = rootNode.findValue("error");
            if (childNode != null) {
                if (childNode != null) {
                    throw new RequestException(rootNode.findValue("error").toString());
                }
            } else {
                nodes = rootNode.findValues("Player");
                for (JsonNode node : nodes) {
                    players.add(mapper.readValue(node, Player.class));
                }
                nodes = rootNode.findValues("Score");
                for (int i = 0; i < nodes.size(); i++) {
                    JsonNode node = nodes.get(i);
                    Score score = mapper.readValue(node, Score.class);
                    score.setPlayer(players.get(i));
                    scores.add(score);
                }
            }
        } catch (JsonProcessingException e) {
            throw new RequestException("JSON error", e);
        } catch (IOException e) {
            throw new RequestException("IO error", e);
        }
        return scores;
    }

    @Override
    public double getAverageScore(String gameId) throws RequestException {
    	return getAverageScore(gameId, "", "", "", -1);
    }
    
    public double getAverageScore(String gameId, String startDate, String endDate, String platform, int difficulty) throws RequestException {
    	Petition request = null;
        String rawResponse = null;
        String value = null ;
        double response = 0.f;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "getAverageScore", this.apiKey);
            value = gameId ; 						request.addParameter("game_id", value);
            value = "json";							request.addParameter("response", value);
            value = startDate ; 					request.addParameter("start_date", value, true);
            value = endDate ; 						request.addParameter("end_date", value, true);
            value = platform ; 						request.addParameter("platform", value, true);
            if ( difficulty != 0 )            	
            	value = Tools.NumericToString(difficulty);	request.addParameter("difficulty", value, true);      
            
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "getAverageScore", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value", e);
        }

        // Send the request and get the JSON response.
        try {
            rawResponse = this.client.doPost(request);
        } catch (IOException e) {
            throw new RequestException("Something went wrong sending the request", e);
        }

        // Map the JSON response to the model object.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode;
        JsonNode childNode = null;

        try {
            rootNode = mapper.readTree(rawResponse);

            childNode = rootNode.findValue("average_score");
            if (childNode != null) {
                response = childNode.asDouble();
            } else {
                childNode = rootNode.findValue("error");
                if (childNode != null) {
                    throw new RequestException(rootNode.findValue("error").toString());
                }
            }
        } catch (JsonProcessingException e) {
            throw new RequestException("JSON error", e);
        } catch (IOException e) {
            throw new RequestException("IO error", e);
        }

        return response;
    }

}
