package com.jscoreoid.api.game;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.jscoreoid.api.AbstractFacade;
import com.jscoreoid.exceptions.RequestException;
import com.jscoreoid.http.Petition;
import com.jscoreoid.model.EnumToField;
import com.jscoreoid.model.Game;
import com.jscoreoid.model.GameFields;
import com.jscoreoid.model.GameStatFields;
import com.jscoreoid.model.Notification;
import com.jscoreoid.model.OrderBy;
import com.jscoreoid.model.Player;
import com.jscoreoid.model.Tools;


/**
 * Implementation of the interface required to ask the scoreoid Game API.
 * 
 * @author Alejandro Martínez Vieites
 * @author Alexandre Herbreteau
 */
public class GameFacade extends AbstractFacade implements GameInterface {

    /**
     * Constructor for asking the Games part of the API.
     * 
     * @param url the URL of the scoreoid API (https://www.scoreoid.com/api/)
     * @param apiKey the API key scoreoid gave to you
     */
    public GameFacade(String url, String apiKey) {
        super(url, apiKey);        
    }

    public Game getGame(String gameId) throws RequestException {
        Petition request = null;
        String rawResponse = null;
        String value = null;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "getGame", this.apiKey);
            value = gameId;            			request.addParameter("game_id", value);            
            value = "json";            			request.addParameter("response", value);
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "getGame", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value: " + value, e);
        }

        // Send the request and get the JSON response.
        try {
            rawResponse = this.client.doPost(request);
        } catch (IOException e) {
            throw new RequestException("Something went wrong sending the request", e);
        }

        // Map the JSON response to the model object.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = null;
        JsonNode gameNode = null;
        Game game = null;
        try {
            rootNode = mapper.readTree(rawResponse);
            gameNode = rootNode.findValue("Game");
            if (gameNode != null) {
                game = mapper.readValue(gameNode, Game.class);
            } else {
                throw new RequestException("Request error: "+ rootNode.findValue("error"));
            }
        } catch (JsonProcessingException e) {
            throw new RequestException("JSON error", e);
        } catch (IOException e) {
            throw new RequestException("IO error", e);
        }

        return game;
    }

    public String getGameField(String gameId, GameFields field) {
    	 Petition request = null;
         String rawResponse = null;
         String value = null;
         String gameField = null ;

         // Build the request.
         try {
             request = new Petition(this.requestUrl + "getGameField", this.apiKey);
             value = gameId;            			request.addParameter("game_id", value);            
             value = "json";            			request.addParameter("response", value);
             value = EnumToField.getValue(field);	request.addParameter("field", value);
         } catch (MalformedURLException e) {
             throw new RequestException("Bad URL: " + this.requestUrl + "getGameField", e);
         } catch (UnsupportedEncodingException e) {
             throw new RequestException("Bad encoding for parameter value: " + value, e);
         }

         // Send the request and get the JSON response.
         try {
             rawResponse = this.client.doPost(request);
         } catch (IOException e) {
             throw new RequestException("Something went wrong sending the request", e);
         }

         // Map the JSON response to the model object.
         ObjectMapper mapper = new ObjectMapper();
         JsonNode rootNode = null;
         JsonNode gameFieldNode = null;
         try {
             rootNode = mapper.readTree(rawResponse);
             gameFieldNode = rootNode.findValue(EnumToField.getValue(field));
             if (gameFieldNode != null) {            	 
            	 gameField = gameFieldNode.asText() ;
             } else {
                 throw new RequestException("Request error: "+ rootNode.findValue("error"));
             }
         } catch (JsonProcessingException e) {
             throw new RequestException("JSON error", e);
         } catch (IOException e) {
             throw new RequestException("IO error", e);
         }

         return gameField;        
    }

    public List<Player> getPlayers(String gameId, OrderBy orderBy, int startingFrom, int numberOFPlayers, String startDate, String endDate, String platform) {
        Petition request = null;
        String rawResponse = null;
        String value = null;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "getPlayers", this.apiKey);
            value = gameId;            				request.addParameter("game_id", value);            
            value = "json";            				request.addParameter("response", value);
            value = EnumToField.getValue(orderBy);	request.addParameter("order", value, true);
            
            if ( startingFrom > 0 ) value = startingFrom + "," + numberOFPlayers ;
            else value = Tools.NumericToString(numberOFPlayers) ;
            										request.addParameter("limit", value, false );
            value = startDate ;			            request.addParameter("start_date", value, false );
            value = endDate ;			            request.addParameter("end_date", value, false );
            value = platform ;			            request.addParameter("platform", value, false );
            
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "getPlayers", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value: " + value, e);
        }

        // Send the request and get the JSON response.
        try {
            rawResponse = this.client.doPost(request);
        } catch (IOException e) {
            throw new RequestException("Something went wrong sending the request", e);
        }

        // Map the JSON response to the model object.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = null;
        List<Player> players = new ArrayList<Player>();
        try {
            rootNode = mapper.readTree(rawResponse);
            if (rootNode != null) {
                List<JsonNode> nodes = rootNode.findValues("Player");
                for (JsonNode node : nodes) {
                	Player p = mapper.readValue(node, Player.class);
                	p.deserializeLists();
                    players.add(p);                    
                }
                if (nodes == null || nodes.size() == 0) {
                    JsonNode error = rootNode.findValue("error");
                    if (error != null) {
                        throw new RequestException("Request error: "+ rootNode.findValue("error"));
                    }
                }
                
            }
        } catch (JsonProcessingException e) {
            throw new RequestException("JSON error", e);
        } catch (IOException e) {
            throw new RequestException("IO error", e);
        }
        return players;
    }
    
    @Override
    public List<Player> getPlayers(String gameId) {
    	return getPlayers(gameId, OrderBy.DESC, -1, -1, "", "", "");
    }
    
    @Override
    public double getGameAverage(String gameId, GameStatFields field, String startDate, String endDate, String platform) throws RequestException {    	
    	Petition request = null;
        String rawResponse = null;
        String value = null;
        double gameField = 0.f ;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "getGameAverage", this.apiKey);
            value = gameId;	            			request.addParameter("game_id", value);
            value = "json";	            			request.addParameter("response", value);
            value = EnumToField.getValue(field);	request.addParameter("field", value);
            value = startDate ;	        			request.addParameter("start_date", value, true);
            value = endDate ;           			request.addParameter("end_date", value, true);
            value = platform ;          			request.addParameter("platform", value, true);
            
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "getGameAverage", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value: " + value, e);
        }

        // Send the request and get the JSON response.
        try {
            rawResponse = this.client.doPost(request);
        } catch (IOException e) {
            throw new RequestException("Something went wrong sending the request", e);
        }

        // Map the JSON response to the model object.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = null;
        JsonNode gameFieldNode = null;
        try {
            rootNode = mapper.readTree(rawResponse);
            gameFieldNode = rootNode.findValue(EnumToField.getValue(field));
            if (gameFieldNode != null) {            	 
           	 gameField = gameFieldNode.asDouble() ;
            } else {
                throw new RequestException("Request error: "+ rootNode.findValue("error"));
            }
        } catch (JsonProcessingException e) {
            throw new RequestException("JSON error", e);
        } catch (IOException e) {
            throw new RequestException("IO error", e);
        }
        return gameField;
    }
    
    public double getGameAverage(String gameId, GameStatFields field) {
    	return getGameAverage(gameId, field, "", "", "");
    }

    public double getGameTop(String gameId, GameStatFields field) {
    	return getGameTop(gameId, field, "", "", "");
    }
    
    @Override
    public double getGameTop(String gameId, GameStatFields field, String startDate, String endDate, String platform) throws RequestException {
    	
    	Petition request = null;
        String rawResponse = null;
        String value = null;
        double gameField = 0.f ;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "getGameTop", this.apiKey);
            value = gameId;	            			request.addParameter("game_id", value);
            value = "json";	            			request.addParameter("response", value);
            value = EnumToField.getValue(field);	request.addParameter("field", value);
            value = startDate ;	        			request.addParameter("start_date", value, true);
            value = endDate ;           			request.addParameter("end_date", value, true);
            value = platform ;          			request.addParameter("platform", value, true);
            
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "getGameTop", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value: " + value, e);
        }

        // Send the request and get the JSON response.
        try {
            rawResponse = this.client.doPost(request);
        } catch (IOException e) {
            throw new RequestException("Something went wrong sending the request", e);
        }

        // Map the JSON response to the model object.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = null;
        JsonNode gameFieldNode = null;
        try {
            rootNode = mapper.readTree(rawResponse);
            gameFieldNode = rootNode.findValue(EnumToField.getValue(field));
            if (gameFieldNode != null) {            	 
           	 gameField = gameFieldNode.asDouble() ;
            } else {
                throw new RequestException("Request error: "+ rootNode.findValue("error"));
            }
        } catch (JsonProcessingException e) {
            throw new RequestException("JSON error", e);
        } catch (IOException e) {
            throw new RequestException("IO error", e);
        }
        return gameField;
    }

    public double getGameLowest(String gameId, GameStatFields field) {
    	return getGameLowest(gameId, field, "", "", "");
    }

    @Override
    public double getGameLowest(String gameId, GameStatFields field, String startDate, String endDate, String platform)	throws RequestException {
    	Petition request = null;
        String rawResponse = null;
        String value = null;
        double gameField = 0.f ;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "getGameLowest", this.apiKey);
            value = gameId;	            			request.addParameter("game_id", value);
            value = "json";	            			request.addParameter("response", value);
            value = EnumToField.getValue(field);	request.addParameter("field", value);
            value = startDate ;	        			request.addParameter("start_date", value, true);
            value = endDate ;           			request.addParameter("end_date", value, true);
            value = platform ;          			request.addParameter("platform", value, true);
            
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "getGameLowest", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value: " + value, e);
        }

        // Send the request and get the JSON response.
        try {
            rawResponse = this.client.doPost(request);
        } catch (IOException e) {
            throw new RequestException("Something went wrong sending the request", e);
        }

        // Map the JSON response to the model object.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = null;
        JsonNode gameFieldNode = null;
        try {
            rootNode = mapper.readTree(rawResponse);
            gameFieldNode = rootNode.findValue(EnumToField.getValue(field));
            if (gameFieldNode != null) {            	 
           	 gameField = gameFieldNode.asDouble() ;
            } else {
                throw new RequestException("Request error: "+ rootNode.findValue("error"));
            }
        } catch (JsonProcessingException e) {
            throw new RequestException("JSON error", e);
        } catch (IOException e) {
            throw new RequestException("IO error", e);
        }
        return gameField;
    }
    
    public double getGameTotal(String gameId, GameStatFields field) {
    	return getGameTotal(gameId, field, "", "", "");
    }
    
    @Override
    public double getGameTotal(String gameId, GameStatFields field, String startDate, String endDate, String platform) throws RequestException {
    	Petition request = null;
        String rawResponse = null;
        String value = null;
        double gameField = 0.f ;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "getGameTotal", this.apiKey);
            value = gameId;	            			request.addParameter("game_id", value);
            value = "json";	            			request.addParameter("response", value);
            value = EnumToField.getValue(field);	request.addParameter("field", value);
            value = startDate ;	        			request.addParameter("start_date", value, true);
            value = endDate ;           			request.addParameter("end_date", value, true);
            value = platform ;          			request.addParameter("platform", value, true);
            
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "getGameTotal", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value: " + value, e);
        }

        // Send the request and get the JSON response.
        try {
            rawResponse = this.client.doPost(request);
        } catch (IOException e) {
            throw new RequestException("Something went wrong sending the request", e);
        }

        // Map the JSON response to the model object.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = null;
        JsonNode gameFieldNode = null;
        try {
            rootNode = mapper.readTree(rawResponse);
            gameFieldNode = rootNode.findValue(EnumToField.getValue(field));
            if (gameFieldNode != null) {            	 
           	 gameField = gameFieldNode.asDouble() ;
            } else {
                throw new RequestException("Request error: "+ rootNode.findValue("error"));
            }
        } catch (JsonProcessingException e) {
            throw new RequestException("JSON error", e);
        } catch (IOException e) {
            throw new RequestException("IO error", e);
        }
        return gameField;
    }

    public List<Notification> getNotification(String gameId) {
    	Petition request = null;
        String rawResponse = null;
        String value = null;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "getNotification", this.apiKey);
            value = gameId;            				request.addParameter("game_id", value);            
            value = "json";            				request.addParameter("response", value);                        
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "getNotification", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value: " + value, e);
        }       	    	

        // Send the request and get the JSON response.
        try {
            rawResponse = this.client.doPost(request);
        } catch (IOException e) {
            throw new RequestException("Something went wrong sending the request", e);
        }

        // Map the JSON response to the model object.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = null;
        List<Notification> notifications = new ArrayList<Notification>();
        try {
            rootNode = mapper.readTree(rawResponse);
            if (rootNode != null) {
            	JsonNode nodeN = rootNode.findValue("notifications").findValue("game_notification");
            	List<JsonNode> nodes = nodeN.findValues("GameNotification");
                for (JsonNode node : nodes) {
                	notifications.add(mapper.readValue(node, Notification.class));
                }
                if (nodes == null || nodes.size() == 0) {
                    JsonNode error = rootNode.findValue("error");
                    if (error != null) {
                        throw new RequestException("Request error: "+ rootNode.findValue("error"));
                    }
                }
                
            }
        } catch (JsonProcessingException e) {
            throw new RequestException("JSON error", e);
        } catch (IOException e) {
            throw new RequestException("IO error", e);
        }
        return notifications;
    }

    @Override
	public String getGameData(String gameId, String key) throws RequestException {
    	return getGameData(gameId, key, "");
    }
    
	@Override
	public String getGameData(String gameId, String key, String defaultValue) throws RequestException {
		Petition request = null;
        String rawResponse = null;
        String value = null;
        String gameData = null ;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "getGameData", this.apiKey);
            value = gameId;							request.addParameter("game_id", value);
            value = "json";         				request.addParameter("response", value);
            value = key;							request.addParameter("key", value );
            value = defaultValue;					request.addParameter("default", value );
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "getGameData", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value: " + value, e);
        }

        // Send the request and get the JSON response.
        try {
            rawResponse = this.client.doPost(request);
        } catch (IOException e) {
            throw new RequestException("Something went wrong sending the request", e);
        }

        // Map the JSON response to the model object.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = null;
        JsonNode keyNode = null;
        try {
            rootNode = mapper.readTree(rawResponse);
            keyNode = rootNode.findValue(key);
            if (keyNode != null) {            	 
           	 gameData = keyNode.asText() ;
            } else {
                throw new RequestException("Request error: "+ rootNode.findValue("error"));
            }
        } catch (JsonProcessingException e) {
            throw new RequestException("JSON error", e);
        } catch (IOException e) {
            throw new RequestException("IO error", e);
        }

        return gameData; 
	}

	@Override
	public boolean setGameData(String gameId, String key, String value) throws RequestException {
		Petition request = null;
        String valueTmp = null;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "setGameData", this.apiKey);
            valueTmp = gameId;						request.addParameter("game_id", valueTmp);
            valueTmp = "json";         				request.addParameter("response", valueTmp);
            valueTmp = key;							request.addParameter("key", valueTmp );
            valueTmp = value;						request.addParameter("value", valueTmp );
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "setGameData", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value: " + valueTmp, e);
        }
        
        return Tools.getBooleanAnswer(request, client); 
	}

	@Override
	public boolean unsetGameData(String gameId, String key) throws RequestException {
		Petition request = null;
        String value = null;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "unsetGameData", this.apiKey);
            value = gameId;							request.addParameter("game_id", value);
            value = "json";         				request.addParameter("response", value);
            value = key;							request.addParameter("key", value );	            
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "unsetGameData", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value: " + value, e);
        }
        
        return Tools.getBooleanAnswer(request, client);
	}
}
