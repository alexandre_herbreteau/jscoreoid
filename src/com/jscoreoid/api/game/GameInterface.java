package com.jscoreoid.api.game;

import java.util.List;

import com.jscoreoid.exceptions.RequestException;
import com.jscoreoid.model.Game;
import com.jscoreoid.model.GameFields;
import com.jscoreoid.model.GameStatFields;
import com.jscoreoid.model.Notification;
import com.jscoreoid.model.OrderBy;
import com.jscoreoid.model.Player;


/**
 * Interface for the Game API operations.
 * 
 * @author Alejandro Martínez Vieites
 */
public interface GameInterface {

    /**
     * Get all the information about the specified {@link Game}.
     * 
     * @param gameId the scoreoid ID of the game
     * @return the {@link Game} object or <code>null</code> there is not that game
     */
    Game getGame(String gameId) throws RequestException;

    /**
     * Get just the value of the field specified.
     * 
     * @param gameId the scoreoid ID of the game
     * @param field the name of the field
     * @return the field value
     */
    String getGameField(String gameId, GameFields field) throws RequestException;

    List<Player> getPlayers(String gameId) throws RequestException;
    List<Player> getPlayers(String gameId, OrderBy orderBy, int startingFrom, int numberOFPlayers, String startDate, String endDate, String platform) throws RequestException;

    double getGameAverage(String gameId, GameStatFields field) throws RequestException;
    double getGameAverage(String gameId, GameStatFields field, String startDate, String endDate, String platform) throws RequestException;

    double getGameTop(String gameId, GameStatFields field) throws RequestException;
    double getGameTop(String gameId, GameStatFields field, String startDate, String endDate, String platform) throws RequestException;

    double getGameLowest(String gameId, GameStatFields field) throws RequestException;
    double getGameLowest(String gameId, GameStatFields field, String startDate, String endDate, String platform) throws RequestException;

    double getGameTotal(String gameId, GameStatFields field) throws RequestException;
    double getGameTotal(String gameId, GameStatFields field, String startDate, String endDate, String platform) throws RequestException;

    List<Notification> getNotification(String gameId) throws RequestException;
    
    String getGameData(String gameId, String key) throws RequestException;
    String getGameData(String gameId, String key, String defaultValue) throws RequestException;
    
    boolean setGameData(String gameId, String key, String value) throws RequestException;
    
    boolean unsetGameData(String gameId, String key) throws RequestException; 
}
