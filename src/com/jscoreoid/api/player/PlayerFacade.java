package com.jscoreoid.api.player;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.jscoreoid.api.AbstractFacade;
import com.jscoreoid.exceptions.RequestException;
import com.jscoreoid.http.Petition;
import com.jscoreoid.model.EnumToField;
import com.jscoreoid.model.OrderBy;
import com.jscoreoid.model.Player;
import com.jscoreoid.model.PlayerFields;
import com.jscoreoid.model.Score;
import com.jscoreoid.model.ScoreOrderBy;
import com.jscoreoid.model.Tools;


public class PlayerFacade extends AbstractFacade implements PlayerInterface {
    
    /**
     * Constructor for asking the Players part of the API.
     * 
     * @param url the URL of the scoreoid API (https://www.scoreoid.com/api/)
     * @param apiKey the API key scoreoid gave to you
     */
    public PlayerFacade(String url, String apiKey) {
        super(url, apiKey);
    }

    public boolean createPlayer(String gameId, String username, String password) throws RequestException {
    	Petition request = null;
        String value = null;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "createPlayer", this.apiKey);
            value = gameId;	            request.addParameter("game_id", value);            
            value = "json";	            request.addParameter("response", value);
            value = username;			request.addParameter("username", value);            
            value = password ;			request.addParameter("password", value);                        
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "createPlayer", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value: " + value, e);
        }

        // Send and get answer
        return Tools.getBooleanAnswer(request, client);
    }
    
	@Override
	public boolean editPlayer(String gameId, Player player) throws RequestException {
		Petition request = null;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "editPlayer", this.apiKey);        
            request.addParameter("game_id", gameId);                 
            request.addParameter("response", "json");                       
            request.addParameter("username", player.getUsername());            
            request.addParameter("password", player.getPassword(), true /* optional */);
            request.addParameter("score",Tools.NumericToString(player.getScore()), true /* optional */);
            request.addParameter("difficulty", Tools.NumericToString(player.getDifficulty()), true /* optional */);
            request.addParameter("unique_id", Tools.NumericToString(player.getUniqueId()), true /* optional */);
            request.addParameter("first_name", player.getFirstName(), true /* optional */);
            request.addParameter("last_name", player.getLastName(), true /* optional */);
            request.addParameter("email", player.getEmail(), true /* optional */);
            request.addParameter("created", player.getCreated(), true /* optional */);
            request.addParameter("updated", player.getUpdated(), true /* optional */);
            request.addParameter("bonus", Tools.NumericToString(player.getBonus()), true /* optional */);
            request.addParameter("achievements", Tools.ListToString(player.getAchievements()), true /* optional */);
            request.addParameter("best_score", Tools.NumericToString(player.getBestScore()), true /* optional */);
            request.addParameter("gold", Tools.NumericToString(player.getGold()), true /* optional */);
            request.addParameter("money", Tools.NumericToString(player.getMoney()), true /* optional */);
            request.addParameter("kills", Tools.NumericToString(player.getKills()), true /* optional */);
            request.addParameter("lives", Tools.NumericToString(player.getLives()), true /* optional */);
            request.addParameter("time_played", Tools.NumericToString(player.getTimePlayed()), true /* optional */);
            request.addParameter("unlocked_levels", Tools.ListToString(player.getUnlockedLevels()), true /* optional */);
            request.addParameter("unlocked_items", Tools.ListToString(player.getUnlockedItems()), true /* optional */);
            request.addParameter("inventory", Tools.ListToString(player.getInventory()), true /* optional */);
            request.addParameter("last_level", Tools.NumericToString(player.getLastLevel()), true /* optional */);
            request.addParameter("current_level", Tools.NumericToString(player.getCurrentLevel()), true /* optional */);
            request.addParameter("current_time", Tools.NumericToString(player.getCurrentTime()), true /* optional */);
            request.addParameter("current_bonus", Tools.NumericToString(player.getCurrentBonus()), true /* optional */);
            request.addParameter("current_kills", Tools.NumericToString(player.getCurrentKills()), true /* optional */);
            request.addParameter("current_achievements", Tools.ListToString(player.getCurrentAchievements()), true /* optional */);
            request.addParameter("current_gold", Tools.NumericToString(player.getCurrentGold()), true /* optional */);
            request.addParameter("current_unlocked_levels", Tools.ListToString(player.getCurrentUnlockedLevels()), true /* optional */);
            request.addParameter("current_unlocked_items", Tools.ListToString(player.getCurrentUnlockedItems()), true /* optional */);
            request.addParameter("current_lives", Tools.NumericToString(player.getCurrentLives()), true /* optional */);
            request.addParameter("xp", Tools.NumericToString(player.getXp()), true /* optional */);
            request.addParameter("energy", Tools.NumericToString(player.getEnergy()), true /* optional */);
            request.addParameter("boost", Tools.NumericToString(player.getBoost()), true /* optional */);
            request.addParameter("latitude", Tools.NumericToString(player.getLatitude()), true /* optional */);
            request.addParameter("longitude", Tools.NumericToString(player.getLongitude()), true /* optional */);
            request.addParameter("game_state", player.getGameState(), true /* optional */);
            request.addParameter("platform", player.getPlatform(), true /* optional */);
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "editPlayer", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value: ", e);
        }

        // Send and get answer
        return Tools.getBooleanAnswer(request, client);
	}

	@Override
	public boolean deletePlayer(String gameId, String username) throws RequestException {
		Petition request = null;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "deletePlayer", this.apiKey);        
            request.addParameter("game_id", gameId);                 
            request.addParameter("response", "json");                       
            request.addParameter("username", username);                       
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "deletePlayer", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value: ", e);
        }

        // Send and get answer
        return Tools.getBooleanAnswer(request, client);
	}

	@Override
	public Player getPlayer(String gameId, String username, String password, String email) throws RequestException {
		Petition request = null;
        String rawResponse = null;
        String value = null;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "getPlayer", this.apiKey);
            value = gameId;	        request.addParameter("game_id", value);
            value = "json"; 		request.addParameter("response", value);
            value = username ; 		request.addParameter("username", value);
            value = password ; 		request.addParameter("password", value, true);
            value = email ; 		request.addParameter("email", value, true);
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "getPlayer", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value: " + value, e);
        }

        // Send the request and get the JSON response.
        try {
            rawResponse = this.client.doPost(request);
        } catch (IOException e) {
            throw new RequestException("Something went wrong sending the request", e);
        }

        // Map the JSON response to the model object.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = null;
        JsonNode gameNode = null;
        Player player = null;
        try {
            rootNode = mapper.readTree(rawResponse);
            gameNode = rootNode.findValue("Player");
            if (gameNode != null) {            	
            	player = mapper.readValue(gameNode, Player.class);
            	player.deserializeLists();
            } else {
                throw new RequestException("Request error: "+ rootNode.findValue("error"));
            }
        } catch (JsonProcessingException e) {
            throw new RequestException("JSON error", e);
        } catch (IOException e) {
            throw new RequestException("IO error", e);
        }

        return player;
	}

	@Override
	public Player getPlayer(String gameId, String username)	throws RequestException {		
		return getPlayer(gameId, username, "", "");
	}

	@Override
	public String getPlayerField(String gameId, String username, PlayerFields field) throws RequestException {
		Petition request = null;
        String rawResponse = null;
        String value = null;
        String playerField = null ;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "getPlayerField", this.apiKey);
            value = gameId;							request.addParameter("game_id", value);
            value = "json";         				request.addParameter("response", value);
            value = username;         				request.addParameter("username", value);
            value = EnumToField.getValue(field);	request.addParameter("field", value );	            
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "getPlayerField", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value: " + value, e);
        }

        // Send the request and get the JSON response.
        try {
            rawResponse = this.client.doPost(request);
        } catch (IOException e) {
            throw new RequestException("Something went wrong sending the request", e);
        }

        // Map the JSON response to the model object.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = null;
        JsonNode playerFieldNode = null;
        try {
            rootNode = mapper.readTree(rawResponse);
            playerFieldNode = rootNode.findValue(EnumToField.getValue(field));
            if (playerFieldNode != null) {            	 
           	 playerField = playerFieldNode.asText() ;
            } else {
                throw new RequestException("Request error: "+ rootNode.findValue("error"));
            }
        } catch (JsonProcessingException e) {
            throw new RequestException("JSON error", e);
        } catch (IOException e) {
            throw new RequestException("IO error", e);
        }

        return playerField; 
	}
	
	@Override
	public long getPlayerRank(String gameId, String username) throws RequestException {		
		return getPlayerRank(gameId, username, "", "", "", -1);
	}

	@Override
	public long getPlayerRank(String gameId, String username, String startDate, String endDate, String platform, int difficulty) throws RequestException {
		Petition request = null;
        String rawResponse = null;
        String value = null ;
        long response = -1l;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "getPlayerRank", this.apiKey);
            value = gameId;					request.addParameter("game_id", value);            
            value = "json" ; 				request.addParameter("response", value);
            value = username;				request.addParameter("username", value);
            value = startDate ;				request.addParameter("start_date", value, true);
            value = endDate ;				request.addParameter("end_date", value, true);
            value = platform ;				request.addParameter("platform", value, true);  
            if ( difficulty != 0 ){
                value = Tools.NumericToString(difficulty);	request.addParameter("difficulty", value, true);
            }
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "getPlayerRank", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value " + value, e);
        }

        // Send the request and get the JSON response.
        try {
            rawResponse = this.client.doPost(request);
        } catch (IOException e) {
            throw new RequestException("Something went wrong sending the request", e);
        }

        // Map the JSON response to the model object.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode;
        JsonNode childNode = null;

        try {
            rootNode = mapper.readTree(rawResponse);

            childNode = rootNode.findValue("rank");
            if (childNode != null) {
                response = childNode.asLong();
            } else {
                childNode = rootNode.findValue("error");
                if (childNode != null) {
                    throw new RequestException(rootNode.findValue("error").toString());
                }
            }
        } catch (JsonProcessingException e) {
            throw new RequestException("JSON error", e);
        } catch (IOException e) {
            throw new RequestException("IO error", e);
        }

        return response;
	}

	@Override
	public boolean updatePlayerField(String gameId, String username, PlayerFields field, String value) throws RequestException {
		Petition request = null;
		String tmpValue = null ;
		
        // Build the request.
        try {
            request = new Petition(this.requestUrl + "updatePlayerField", this.apiKey);        
            tmpValue = gameId;							request.addParameter("game_id", tmpValue);
            tmpValue = "json";         					request.addParameter("response", tmpValue);
            tmpValue = username;       					request.addParameter("username", tmpValue);
            tmpValue = EnumToField.getValue(field);		request.addParameter("field", tmpValue );
            tmpValue = value;         					request.addParameter("value", tmpValue);
            
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "updatePlayerField", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value: " + tmpValue, e);
        }

        // Send and get answer
        return Tools.getBooleanAnswer(request, client);
	}

	@Override
	public List<Score> getPlayerScores(String gameId, String username) throws RequestException {		
		return getPlayerScores(gameId, username, ScoreOrderBy.SCORE, OrderBy.DESC, -1, -1, "", "", "", -1);
	}

	@Override
	public List<Score> getPlayerScores(String gameId, String username, ScoreOrderBy orderBy, OrderBy order, int lowLimit, int numberOfScore, String startDate, String endDate, String platform, int difficulty) throws RequestException{
		Petition request = null;
        String rawResponse = null;
        String value = null ;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "getPlayerScores", this.apiKey);
            value = gameId;								request.addParameter("game_id", value);
            value = "json";								request.addParameter("response", value);
            value = username ;							request.addParameter("username", value);
            value = EnumToField.getValue(orderBy); 		request.addParameter("order_by", value, true);
            value = EnumToField.getValue(order);		request.addParameter("order", value, true);            
            if ( lowLimit == -1 )
            	value = Tools.NumericToString(numberOfScore);            	
            else
            	value = String.valueOf(lowLimit) + "," + String.valueOf(numberOfScore);             
            request.addParameter("limit", value, true);
            value = startDate; 							request.addParameter("start_date", value, true);
            value = endDate; 							request.addParameter("end_date", value, true);
            if ( difficulty != 0 ){
            value = Tools.NumericToString(difficulty);	request.addParameter("difficulty", value, true);
            }
            value = platform; 							request.addParameter("platform", value, true);                      
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "getPlayerScores", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value " + value, e);
        }

        // Send the request and get the JSON response.
        try {
            rawResponse = this.client.doPost(request);
        } catch (IOException e) {
            throw new RequestException("Something went wrong sending the request", e);
        }

        // Map the JSON response to the model object.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode;
        JsonNode childNode = null;
        List<Score> scores = new ArrayList<Score>();
        List<JsonNode> nodes = null;

        try {
            rootNode = mapper.readTree(rawResponse);

            childNode = rootNode.findValue("error");
            if (childNode != null) {
                if (childNode != null) {
                    throw new RequestException(rootNode.findValue("error").toString());
                }
            } else {                
                nodes = rootNode.findValues("Score");
                for (int i = 0; i < nodes.size(); i++) {
                    JsonNode node = nodes.get(i);                                       
                    scores.add(mapper.readValue(node, Score.class));
                }
            }
        } catch (JsonProcessingException e) {
            throw new RequestException("JSON error", e);
        } catch (IOException e) {
            throw new RequestException("IO error", e);
        }
        return scores;
	}

	@Override
	public long countPlayers(String gameId) throws RequestException {		
		return countPlayers(gameId, "", "", "", -1);
	}

	@Override
	public long countPlayers(String gameId, String startDate, String endDate, String platform, int difficulty) throws RequestException {		
		Petition request = null;
        String rawResponse = null;
        String value = null ;
        long response = -1l;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "countPlayers", this.apiKey);
            value = gameId;					request.addParameter("game_id", value);
            value = "json" ; 				request.addParameter("response", value);
            value = startDate ;				request.addParameter("start_date", value, true);
            value = endDate ;				request.addParameter("end_date", value, true);
            value = platform ;				request.addParameter("platform", value, true);
            if ( difficulty != 0 ){
                value = Tools.NumericToString(difficulty);	request.addParameter("difficulty", value, true);
            }
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "countPlayers", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value " + value, e);
        }

        // Send the request and get the JSON response.
        try {
            rawResponse = this.client.doPost(request);
        } catch (IOException e) {
            throw new RequestException("Something went wrong sending the request", e);
        }

        // Map the JSON response to the model object.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode;
        JsonNode childNode = null;

        try {
            rootNode = mapper.readTree(rawResponse);

            childNode = rootNode.findValue("players");
            if (childNode != null) {
                response = childNode.asLong();
            } else {
                childNode = rootNode.findValue("error");
                if (childNode != null) {
                    throw new RequestException(rootNode.findValue("error").toString());
                }
            }
        } catch (JsonProcessingException e) {
            throw new RequestException("JSON error", e);
        } catch (IOException e) {
            throw new RequestException("IO error", e);
        }

        return response;
	}

	@Override
	public String getPlayerData(String gameId, String username, String key) throws RequestException {
		Petition request = null;
        String rawResponse = null;
        String value = null;
        String playerData = null ;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "getPlayerData", this.apiKey);
            value = gameId;							request.addParameter("game_id", value);
            value = "json";         				request.addParameter("response", value);
            value = username;         				request.addParameter("username", value);
            value = key;							request.addParameter("key", value );	            
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "getPlayerData", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value: " + value, e);
        }

        // Send the request and get the JSON response.
        try {
            rawResponse = this.client.doPost(request);
        } catch (IOException e) {
            throw new RequestException("Something went wrong sending the request", e);
        }

        // Map the JSON response to the model object.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = null;
        JsonNode keyNode = null;
        try {
            rootNode = mapper.readTree(rawResponse);
            keyNode = rootNode.findValue(key);
            if (keyNode != null) {            	 
           	 playerData = keyNode.asText() ;
            } else {
                throw new RequestException("Request error: "+ rootNode.findValue("error"));
            }
        } catch (JsonProcessingException e) {
            throw new RequestException("JSON error", e);
        } catch (IOException e) {
            throw new RequestException("IO error", e);
        }

        return playerData; 
	}

	@Override
	public boolean setPlayerData(String gameId, String username, String key, String value) throws RequestException {
		Petition request = null;
        String valueTmp = null;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "setPlayerData", this.apiKey);
            valueTmp = gameId;						request.addParameter("game_id", valueTmp);
            valueTmp = "json";         				request.addParameter("response", valueTmp);
            valueTmp = username;         			request.addParameter("username", valueTmp);
            valueTmp = key;							request.addParameter("key", valueTmp );
            valueTmp = value;						request.addParameter("value", valueTmp );
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "setPlayerData", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value: " + valueTmp, e);
        }
        
        return Tools.getBooleanAnswer(request, client);        
	}

	@Override
	public boolean unsetPlayerData(String gameId, String username, String key) throws RequestException {
		Petition request = null;
        String value = null;

        // Build the request.
        try {
            request = new Petition(this.requestUrl + "unsetPlayerData", this.apiKey);
            value = gameId;							request.addParameter("game_id", value);
            value = "json";         				request.addParameter("response", value);
            value = username;         				request.addParameter("username", value);
            value = key;							request.addParameter("key", value );	            
        } catch (MalformedURLException e) {
            throw new RequestException("Bad URL: " + this.requestUrl + "unsetPlayerData", e);
        } catch (UnsupportedEncodingException e) {
            throw new RequestException("Bad encoding for parameter value: " + value, e);
        }
        
        return Tools.getBooleanAnswer(request, client);
	}
}
