package com.jscoreoid.api.player;

import java.util.List;

import com.jscoreoid.exceptions.RequestException;
import com.jscoreoid.model.OrderBy;
import com.jscoreoid.model.Player;
import com.jscoreoid.model.PlayerFields;
import com.jscoreoid.model.Score;
import com.jscoreoid.model.ScoreOrderBy;

/**
 * Interface for the Player API operations.
 * 
 * @author Alexandre Herbreteau
 */
public interface PlayerInterface {
	
    boolean createPlayer(String gameId, String username, String password) throws RequestException;
    
    boolean editPlayer( String gameId, Player player ) throws RequestException;
    
    boolean deletePlayer(String gameId, String username) throws RequestException;
    
    Player getPlayer(String gameId, String username, String password, String email) throws RequestException;
    Player getPlayer(String gameId, String username) throws RequestException;
    
    String getPlayerField(String gameId, String username, PlayerFields field) throws RequestException;
    
    long getPlayerRank(String gameId, String username) throws RequestException;
    long getPlayerRank(String gameId, String username, String startDate, String endDate, String platform, int difficulty) throws RequestException;    
    
    boolean updatePlayerField(String gameId, String username, PlayerFields field, String value) throws RequestException;
    
    List<Score> getPlayerScores(String gameId, String username) throws RequestException;
    List<Score> getPlayerScores(String gameId, String username, ScoreOrderBy orderBy, OrderBy order, int lowLimit, int numberOfScore, String startDate, String endDate, String platform, int difficulty) throws RequestException;
    
    long countPlayers(String gameId) throws RequestException;
    long countPlayers(String gameId, String startDate, String endDate, String platform, int difficulty) throws RequestException;
    
    String getPlayerData(String gameId, String username, String key) throws RequestException;
    
    boolean setPlayerData(String gameId, String username, String key, String value) throws RequestException;
    
    boolean unsetPlayerData(String gameId, String username, String key) throws RequestException;
        
}
