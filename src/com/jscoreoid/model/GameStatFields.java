package com.jscoreoid.model;

public enum GameStatFields {
	Bonus, 
	Gold, 
	Money,
	Kills,
	Lives, 
	TimePlayed, 
	UnlockedLevels
}
