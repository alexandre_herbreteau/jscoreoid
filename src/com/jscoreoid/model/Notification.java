package com.jscoreoid.model;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonProperty;

public class Notification implements Serializable {

    /** The serial version UID. */
    private static final long serialVersionUID = 4439765674861221289L;
    private String title = null ;
    private String content = null ;
    @JsonProperty("start_date")
    private String startDate = null ;
    @JsonProperty("end_date")
    private String endDate = null ;
    private boolean status = false ;
    private boolean debug = false ;    

    public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public boolean isDebug() {
		return debug;
	}
	public void setDebug(boolean debug) {
		this.debug = debug;
	}
}
