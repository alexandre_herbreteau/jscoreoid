package com.jscoreoid.model;

/**
 * All the fields of a {@link Game}.
 * 
 * @author Alejandro Martinez Vieites
 * @author Alexandre Herbreteau
 */
public enum GameFields { 	UserId,
							Name, 
							ShortDescription, 
							Description, 
							GameType, 
							Version, 
							Levels, 
							Platform, 
							PlayURL, 
							WebsiteURL, 
							Created, 
							Updated, 
							PlayersCount, 
							ScoresCount, 
							Locked, 
							Status } ;