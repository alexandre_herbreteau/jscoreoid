package com.jscoreoid.model;

public enum SimpleOrderBy {
	ASC, DESC
}
