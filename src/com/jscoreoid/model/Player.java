package com.jscoreoid.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class Player implements Serializable {

    /** The serial version UID. */
    private static final long serialVersionUID = -6093667694634796350L;
    private int id = -1;
    //@JsonProperty("game_id")
    //private String gameId = null;
    //@JsonProperty("user_id")
    //private int userId = null;
    private String username = null;
    private String password = null;
    @JsonProperty("unique_id")
    private int uniqueId = -1;
    @JsonProperty("first_name")
    private String firstName = null;
    @JsonProperty("last_name")
    private String lastName = null;
    private String email = null;    
    private int bonus = -1;    
    private String achievements = null ; 
    private List<String> achievementsList = new ArrayList<String>();
    private int gold = -1;
    private int money = -1;
    private int kills = -1;
    private int lives = -1;
    @JsonProperty("time_played")
    private int timePlayed = -1;
    @JsonProperty("unlocked_levels")
    private String unlockedLevels = null ;
    private List<Integer> unlockedLevelsList = new ArrayList<Integer>();
    @JsonProperty("unlocked_items")
    private String unlockedItems = null ;
    private List<String> unlockedItemsList = new ArrayList<String>();
    private String inventory = null ;
    private List<String> inventoryList = new ArrayList<String>();    
    @JsonProperty("last_level")
    private int lastLevel = -1;
    @JsonProperty("current_level")
    private int currentLevel = -1;
    @JsonProperty("current_time")
    private int currentTime = -1;
    @JsonProperty("current_bonus")
    private int currentBonus = -1;
    @JsonProperty("current_kills")
    private int currentKills = -1;
    @JsonProperty("current_achievements")
    private String currentAchievements = null ;
    private List<String> currentAchievementsList = new ArrayList<String>();
    @JsonProperty("current_gold")
    private int currentGold = -1;
    @JsonProperty("current_unlocked_levels")
    private String currentUnlockedLevels = null ;
    private List<Integer> currentUnlockedLevelsList = new ArrayList<Integer>();
    @JsonProperty("current_unlocked_items")
    private String currentUnlockedItems = null ;
    private List<String> currentUnlockedItemsList = new ArrayList<String>();
    @JsonProperty("current_lives")
    private int currentLives = -1;
    private int xp = -1;
    private int energy = -1;
    private int boost = -1;
    private int latitude = -1;
    @JsonProperty("longtitude")
    private int longitude = -1;
    @JsonProperty("game_state")
    private String gameState = null;
    private String platform = null;
    private int rank = -1 ;
    @JsonProperty("best_score")
    private int bestScore = -1;
    private String created = null;
    private String updated = null;
    private int score = -1 ;
    private int difficulty = -1 ;

    public final int getId() {
        return id;
    }

    public final void setId(int id) {
        this.id = id;
    }

    /*public final String getGameId() {
        return gameId;
    }

    public final void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public final int getUserId() {
        return userId;
    }

    public final void setUserId(int userId) {
        this.userId = userId;
    }*/

    public final String getUsername() {
        return username;
    }

    public final void setUsername(String username) {
        this.username = username;
    }

    public final String getPassword() {
        return password;
    }

    public final void setPassword(String password) {
        this.password = password;
    }

    public final int getUniqueId() {
        return uniqueId;
    }

    public final void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public final String getFirstName() {
        return firstName;
    }

    public final void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public final String getLastName() {
        return lastName;
    }

    public final void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public final String getEmail() {
        return email;
    }

    public final void setEmail(String email) {
        this.email = email;
    }

    public final String getCreated() {
        return created;
    }

    public final void setCreated(String created) {
        this.created = created;
    }

    public final String getUpdated() {
        return updated;
    }

    public final void setUpdated(String updated) {
        this.updated = updated;
    }

    public final int getBonus() {
        return bonus;
    }

    public final void setBonus(int bonus) {
        this.bonus = bonus;
    }
    
    public final List<String> getAchievements() {
        return achievementsList;
    }

    public final void addAchievements(String achievements) {
        this.achievementsList.add(achievements);
    }

    public final int getBestScore() {
        return bestScore;
    }

    public final void setBestScore(int bestScore) {
        this.bestScore = bestScore;
    }

    public final int getGold() {
        return gold;
    }

    public final void setGold(int gold) {
        this.gold = gold;
    }

    public final int getMoney() {
        return money;
    }

    public final void setMoney(int money) {
        this.money = money;
    }

    public final int getKills() {
        return kills;
    }

    public final void setKills(int kills) {
        this.kills = kills;
    }

    public final int getLives() {
        return lives;
    }

    public final void setLives(int lives) {
        this.lives = lives;
    }

    public final int getTimePlayed() {
        return timePlayed;
    }

    public final void setTimePlayed(int timePlayed) {
        this.timePlayed = timePlayed;
    }

    public final List<Integer> getUnlockedLevels() {
        return unlockedLevelsList;
    }

    public final void addUnlockedLevels(int unlockedLevels) {
        this.unlockedLevelsList.add(unlockedLevels);
    }

    public final List<String> getUnlockedItems() {
        return unlockedItemsList;
    }

    public final void addUnlockedItems(String unlockedItems) {
        this.unlockedItemsList.add(unlockedItems);
    }

    public final List<String> getInventory() {
        return inventoryList;
    }

    public final void addInventory(String inventory) {
        this.inventoryList.add(inventory);
    }

    public final int getLastLevel() {
        return lastLevel;
    }

    public final void setLastLevel(int lastLevel) {
        this.lastLevel = lastLevel;
    }

    public final int getCurrentLevel() {
        return currentLevel;
    }

    public final void setCurrentLevel(int currentLevel) {
        this.currentLevel = currentLevel;
    }

    public final int getCurrentTime() {
        return currentTime;
    }

    public final void setCurrentTime(int currentTime) {
        this.currentTime = currentTime;
    }

    public final int getCurrentBonus() {
        return currentBonus;
    }

    public final void setCurrentBonus(int currentBonus) {
        this.currentBonus = currentBonus;
    }

    public final int getCurrentKills() {
        return currentKills;
    }

    public final void setCurrentKills(int currentKills) {
        this.currentKills = currentKills;
    }

    public final List<String> getCurrentAchievements() {
        return currentAchievementsList;
    }

    public final void addCurrentAchievements(String currentAchievements) {
        this.currentAchievementsList.add(currentAchievements);
    }

    public final int getCurrentGold() {
        return currentGold;
    }

    public final void setCurrentGold(int currentGold) {
        this.currentGold = currentGold;
    }

    public final List<Integer> getCurrentUnlockedLevels() {
        return currentUnlockedLevelsList;
    }

    public final void addCurrentUnlockedLevels(int currentUnlockedLevels) {
        this.currentUnlockedLevelsList.add(currentUnlockedLevels);
    }

    public final List<String> getCurrentUnlockedItems() {
        return currentUnlockedItemsList;
    }

    public final void addCurrentUnlockedItems(String currentUnlockedItems) {
        this.currentUnlockedItemsList.add(currentUnlockedItems);
    }

    public final int getCurrentLives() {
        return currentLives;
    }

    public final void setCurrentLives(int currentLifes) {
        this.currentLives = currentLifes;
    }

    public final int getXp() {
        return xp;
    }

    public final void setXp(int xp) {
        this.xp = xp;
    }

    public final int getEnergy() {
        return energy;
    }

    public final void setEnergy(int energy) {
        this.energy = energy;
    }

    public final int getBoost() {
        return boost;
    }

    public final void setBoost(int boost) {
        this.boost = boost;
    }

    public final int getLatitude() {
        return latitude;
    }

    public final void setLatitude(int latitude) {
        this.latitude = latitude;
    }

    public final int getLongitude() {
        return longitude;
    }

    public final void setLongitude(int longitude) {
        this.longitude = longitude;
    }

    public final String getGameState() {
        return gameState;
    }

    public final void setGameState(String gameState) {
        this.gameState = gameState;
    }

    public final String getPlatform() {
        return platform;
    }

    public final void setPlatform(String platform) {
        this.platform = platform;
    }

    public static final long getSerialversionuid() {
        return serialVersionUID;
    }

    @Override
    public String toString() {
        return String.valueOf(this.id);
    }

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}

	 public final void deserializeLists(){
	    	achievementsList = Tools.StringToList(achievements);	    	
	    	unlockedLevelsList = Tools.StringToListInt(unlockedLevels);
	    	unlockedItemsList = Tools.StringToList(unlockedItems);
	    	inventoryList = Tools.StringToList(inventory);
	    	currentAchievementsList = Tools.StringToList(currentAchievements);	    	
	    	currentUnlockedLevelsList = Tools.StringToListInt(currentUnlockedLevels);
	    	currentUnlockedItemsList = Tools.StringToList(currentUnlockedItems);
	 }
	 
}
