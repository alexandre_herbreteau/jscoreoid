package com.jscoreoid.model;

public enum ScoreOrderBy {
	DATE, SCORE, DATE_SCORE, SCORE_DATE
}
