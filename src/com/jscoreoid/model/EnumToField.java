package com.jscoreoid.model;

/**
 * Converted to get field name according to an enumerate
 * 
 * @author Alexandre Herbreteau
 */

public class EnumToField {
	
	public static String getValue(GameFields v){
		switch (v){
			case UserId 			: return "user_id" ;
			case Name 				: return "name" ;
			case ShortDescription 	: return "short_description" ;
			case Description 		: return "description" ;
			case GameType 			: return "game_type" ;
			case Version			: return "version" ;
			case Levels				: return "levels" ;
			case Platform			: return "platform" ; 
			case PlayURL			: return "play_url" ;
			case WebsiteURL			: return "website_url" ;
			case Created			: return "created" ;
			case Updated			: return "updated" ;
			case PlayersCount		: return "players_count" ;
			case ScoresCount		: return "scores_count" ;
			case Locked				: return "locked";
			case Status				: return "status" ;
		}
		return null ;
	}
	
	public static String getValue(OrderBy v){
		switch(v){
			case ASC 				: return "asc" ;
			case DESC 				: return "desc" ;
			case ASC_ASC 			: return "asc,asc" ;
			case ASC_DESC			: return "asc,desc" ;
			case DESC_DESC			: return "desc,desc" ;
			case DESC_ASC 			: return "desc,asc" ;
		}
		return null ;
	}
	
	public static String getValue(SimpleOrderBy v){
		switch(v){
		case ASC 				: return "asc" ;
		case DESC 				: return "desc" ;		
		}
		return null ;
	} 
	
	public static String getValue(ScoreOrderBy v){
		switch(v){
		case DATE 				: return "date" ;
		case SCORE 				: return "score" ;
		case DATE_SCORE 		: return "date_score" ;
		case SCORE_DATE			: return "score_date" ;
		}
		return null ;
	} 
	
	public static String getValue(GameStatFields v){
		switch(v){
			case Bonus 				: return "bonus" ;
			case Gold 				: return "gold" ;
			case Money 				: return "money" ;
			case Kills				: return "kills" ;
			case Lives				: return "lives" ;
			case TimePlayed 		: return "time_played" ;
			case UnlockedLevels 	: return "unlocked_levels" ;
		}
		return null ;
	}
	
	public static String getValue(PlayerFields v){
		switch(v){
			case Username			: return "username" ; 
			case Password			: return "password" ;
			case UniqueId			: return "unique_id" ;
			case FirstName			: return "first_name" ;
			case LastName			: return "last_name" ;
			case Email				: return "email" ;
			case Platform			: return "platform" ;
			case Created			: return "created" ;
			case Updated			: return "updated" ;
			case Bonus				: return "bonus" ;
			case Achievements		: return "achievements" ;
			case BestScore			: return "best_score" ;
			case Gold				: return "gold" ;
			case Money				: return "money" ;
			case Kills				: return "kills" ;
			case Lives				: return "lives" ;
			case TimePlayed			: return "time_played" ;
			case UnlockedLevels		: return "unlocked_levels" ;
			case UnlockedItems		: return "unlocked_items" ;
			case Inventory			: return "inventory" ;
			case LastLevel			: return "last_level" ;
			case CurrentLevel		: return "current_level" ;
			case CurrentTime		: return "current_time" ;
			case CurrentBonus		: return "current_bonus" ;
			case CurrentKills		: return "current_kills" ;
			case CurrentAchievements: return "current_achievements" ;
			case CurrentGold		: return "current_gold" ;
			case CurrentUnlockedLevels: return "current_unlocked_levels" ;
			case CurrentUnlockedItems: return "current_unlocked_items" ;
			case CurrentLives		: return "current_lives" ;
			case XP					: return "xp" ;
			case Energy				: return "energy" ;
			case Boost				: return "boost" ; 
			case Latitude			: return "latitude" ;
			case Longitude			: return "longitude" ;
			case GameState			: return "game_state" ;
			case Rank				: return "rank" ;
			case Score				: return "score" ;
			case Difficulty			: return "difficulty" ;
	}
	return null ;
} 
}
