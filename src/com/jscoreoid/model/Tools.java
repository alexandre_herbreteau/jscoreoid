package com.jscoreoid.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.jscoreoid.exceptions.RequestException;
import com.jscoreoid.http.HttpClient;
import com.jscoreoid.http.Petition;

public class Tools {
	public static String ListToString(List<?> list){
		StringBuilder result = new StringBuilder();
		if ( list != null ){
			for (Object s : list){
				result.append(s);
				if ( s != list.get(list.size()-1)){
					result.append(",");
				}
			}
		}
		return result.toString() ;
	}
	
	public static List<String> StringToList(String str){		
		return Arrays.asList(str.split("\\s*,\\s*")); 
	}
	
	public static List<Integer> StringToListInt(String str){		
		
		List<Integer> result = new ArrayList<Integer>();
        for(String stringValue : Arrays.asList(str.split("\\s*,\\s*"))) {
            try {
                //Convert String to Integer, and store it into integer array list.
                result.add(Integer.parseInt(stringValue));
            } catch(NumberFormatException nfe) {
               //System.out.println("Could not parse " + nfe);
                System.out.println("Parsing failed! " + stringValue + " can not be an integer");
            } 
        }       
        return result;
  	}
	
	public static String NumericToString(int value){
		if (value == -1){
			return "" ;
		}else{
			return String.valueOf(value);
		}
	}
	
	public static boolean getBooleanAnswer(Petition request, HttpClient client){
		
		String rawResponse = null ;
		
		// Send the request and get the JSON response.
        try {
            rawResponse = client.doPost(request);
        } catch (IOException e) {
            throw new RequestException("Something went wrong sending the request", e);
        }
                
		// Map the JSON response to the model object.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode;
        JsonNode childNode = null;

        try {
            rootNode = mapper.readTree(rawResponse);

            childNode = rootNode.findValue("success");
            if (childNode != null) {
                return true;
            } else {
                childNode = rootNode.findValue("error");
                if (childNode != null) {
                    throw new RequestException(rootNode.findValue("error").toString());
                }
            }
        } catch (JsonProcessingException e) {
            throw new RequestException("JSON error", e);
        } catch (IOException e) {
            throw new RequestException("IO error", e);
        }
        
        return false ;
	}
}
