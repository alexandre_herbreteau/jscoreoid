package com.jscoreoid.model;

public enum OrderBy {
	ASC, DESC, ASC_ASC, ASC_DESC, DESC_ASC, DESC_DESC
}
