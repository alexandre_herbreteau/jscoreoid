package com.jscoreoid.model;

public enum PlayerFields {	Username, 
							Password,
							UniqueId,
							FirstName,
							LastName,
							Email,
							Platform,
							Created,
							Updated,
							Bonus,
							Achievements,
							BestScore,
							Gold,
							Money,
							Kills,
							Lives,
							TimePlayed,
							UnlockedLevels,
							UnlockedItems,
							Inventory,
							LastLevel,
							CurrentLevel,
							CurrentTime,
							CurrentBonus,
							CurrentKills,
							CurrentAchievements,
							CurrentGold,
							CurrentUnlockedLevels,
							CurrentUnlockedItems,
							CurrentLives,
							XP,
							Energy,
							Boost, 
							Latitude,
							Longitude,
							GameState,
							Rank,
							Score,
							Difficulty
}
