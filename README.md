jScoreoid
=========

Java library for the Scoreoid API by 
        . @amvieites (2012) 
	. Alexandre Herbreteau (alexandre.herbreteau@gmail.com) (2013).

1) Download source files and import them by using the eclipse project.
2) or simply feel free to use the jScoreoid.jar library into your project.

Jackson libraries 1.9.12 for JSON parsing are provided under Apache licence.
- jackson-core-asl-1.9.12
- jackson-mapper-asl-1.9.12
